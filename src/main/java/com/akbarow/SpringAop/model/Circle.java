package com.akbarow.SpringAop.model;

public class Circle {

	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		System.out.println("setName method");
	}
	
	public String returnString(String name){
		return name;
		
	}
	
}
