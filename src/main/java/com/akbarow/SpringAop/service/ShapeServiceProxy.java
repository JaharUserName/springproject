package com.akbarow.SpringAop.service;

import com.akbarow.SpringAop.aspect.LoggingAspect;
import com.akbarow.SpringAop.model.Circle;

public class ShapeServiceProxy extends ShapeService {

	@Override
	public Circle getCircle() {
		new LoggingAspect().loggingAdvice();
		return super.getCircle();
	}
	
}
