package com.akbarow.SpringAop;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.akbarow.SpringAop.service.FactoryService;
import com.akbarow.SpringAop.service.ShapeService;

public class App 
{
    public static void main( String[] args )
    {
//    	ApplicationContext context = new ClassPathXmlApplicationContext("spring.xml");
    	
//    	ShapeService shapeService = context.getBean("shapeService", ShapeService.class);
    	
    	FactoryService factoryService = new FactoryService();
    	ShapeService shapeService	= (ShapeService) factoryService.getBean("shapeService");
    	shapeService.getCircle();
    }
}
