package com.akbarow.SpringAop.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import com.akbarow.SpringAop.model.Circle;
import com.akbarow.SpringAop.model.Triangle;
import com.akbarow.SpringAop.service.ShapeService;


public class LoggingAspect {

	
	public Object myAroundAdvice(ProceedingJoinPoint proceedingJoinPoint){
		Object returnValue = null;
		
		try {
			System.out.println("Before ");
			returnValue = proceedingJoinPoint.proceed();
			System.out.println("After returning");
		} catch (Throwable e) {
			System.out.println("After throwing");
			e.printStackTrace();
		}
		
		System.out.println("After Finally");
		
		return returnValue;
	}
	
	
	public void allGetters(){}
		
	public void loggingAdvice(){
		System.out.println("Logging from the advice");
	}
	
}
